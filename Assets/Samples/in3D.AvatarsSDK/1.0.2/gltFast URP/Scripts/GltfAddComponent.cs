﻿using GLTFast;
using in3D.AvatarsSDK.Configurations;
using UnityEngine;

public class GltfAddComponent : MonoBehaviour
{
    [SerializeField] private AvatarsServer server;
    [SerializeField] private UserConfiguration user;
    [SerializeField] private AvatarConfiguration avatar;

    public void AddGltfComponent()
    {
        LoadAvatar();
    }
    
    private async void LoadAvatar()
    {
        var urls = await server.UserAvatar.GetAvatarUrls(user, avatar.Format, avatar.AvatarId);
        
        var gltfast = this.gameObject.AddComponent<GltfAsset>();
        gltfast.url = urls.GetMainUrl();
    }
}

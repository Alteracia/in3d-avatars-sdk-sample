﻿using System;
using in3D.AvatarsSDK.Configurations;
using UnityEngine;
using UnityEngine.Events;

public class ReadLastUserAvatar : MonoBehaviour
{
    [SerializeField] private AvatarsServer server;
    [SerializeField] private UserConfiguration user;

    [Serializable]
    public class StringEvent : UnityEvent<string> {}

    public StringEvent onAvatarIdRead = new StringEvent();

    public void ReadLastAvatar()
    {
        Read();
        
        async void Read()
        {
            var ids = await server.UserAvatar.GetAvatarsIds(user);
            onAvatarIdRead.Invoke($"{{\"id\":\"{ids[ids.Length - 1]}\"}}");
        }
    }
}
